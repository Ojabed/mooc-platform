from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('daftar.urls', namespace='daftar')),
    path('api/', include('api.urls', namespace='api')),
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('api/authentication/', include('users.urls', namespace='users')),
]
