import React, { useEffect, useState } from 'react';
import './App.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import ListIcon from '@material-ui/icons/List';
function Landing() {

    return (
        <Container maxWidth="sm">
            <CssBaseline />


            <Paper align="center">
                Apologies
					<Typography
                    component="h1"
                    variant="h2"
                    align="center"
                    color="textPrimary"
                    gutterBottom
                >
                    Hi
					</Typography>
                {"Here's the worst landing page ever :) , Sorry...I was short on time. at least it's better than copying an existing page, that counts...no? XD"}
                <Typography>
                    <Link
                        color="textPrimary"
                        href={'/courses/'}
                    ><br/><br/>
                        <ListIcon></ListIcon>
                        Click Me to access the course list
                        <ListIcon></ListIcon>
                    </Link></Typography>
            </Paper>

        </Container>
    );
}

export default Landing;
