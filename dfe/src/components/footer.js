import React from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
    footer: {
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(6),
    },
  }));

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://daftar.herokuapp.com"> 
            {/* hoping to deploy on heroku*/}
                Daftar
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

function Footer() {
    const classes = useStyles();
    return (
        <React.Fragment>
            <Container maxWidth="md" component="footer" className={classes.footer}>
                <footer className={classes.footer}>
                    <Typography variant="h6" align="center" gutterBottom>
                    Hey!
                    </Typography>
                    <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
                    You'll find out when you reach the top, you're on the bottom !
                    </Typography>
                    <Copyright />
                </footer>
            </Container>
        </React.Fragment>
    );
}

export default Footer;

