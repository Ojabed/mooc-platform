import React, { useState } from 'react';
import axiosInstance from '../../axios';
import { useHistory } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const useStyles = makeStyles((theme) => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main,
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(3),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

export default function Create() {
	function slugify(string) {
		const a =
			'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;';
		const b =
			'aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------';
		const p = new RegExp(a.split('').join('|'), 'g');

		return string
			.toString()
			.toLowerCase()
			.replace(/\s+/g, '-') // Replace spaces with -
			.replace(p, (c) => b.charAt(a.indexOf(c))) // Replace special characters
			.replace(/&/g, '-and-') // Replace & with 'and'
			.replace(/[^\w\-]+/g, '') // Remove all non-word characters
			.replace(/\-\-+/g, '-') // Replace multiple - with single -
			.replace(/^-+/, '') // Trim - from start of text
			.replace(/-+$/, ''); // Trim - from end of text
	}
	//

	const history = useHistory();
	const initialFormData = Object.freeze({
		name: '',
		slug: '',
		suqare_image: '',
		square_image_url: '',
		starts_at: '',
		description: '',
		instructor: '',
		category: ''
	});

	const [courseData, updateFormData] = useState(initialFormData);
	const [courseimage, setCourseImage] = useState(null);

	const handleChange = (e) => {
		if ([e.target.name] == 'suqare_image') {
			setCourseImage({
				image: e.target.files,
			});
		}
		if ([e.target.name] == 'name') {
			updateFormData({
				...courseData,
				[e.target.name]: e.target.value.trim(),
				['slug']: slugify(e.target.value.trim()),
			});
		} else {
			updateFormData({
				...courseData,
				[e.target.name]: e.target.value.trim(),
			});
		}
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		let formData = new FormData();
		formData.append('name', courseData.name);
		formData.append('slug', courseData.slug);
		formData.append('category', courseData.category);
		formData.append('instructor', courseData.instructor);
		formData.append('description', courseData.description);
		formData.append('starts_at', courseData.starts_at);
		formData.append('square_image_url', courseData.square_image_url);
		formData.append('suqare_image', courseimage.image[0]);
		axiosInstance.post(`course/`, formData).then((res)=>{		
			history.push({
				pathname: '/',
			});
			window.location.reload();}).catch((err) => {
			for (var key in err.response.data){
				toast(`${key} : ${err.response.data[key][0]}`)}});

	};

	const classes = useStyles();

	return (
		<Container component="main" maxWidth="xs">
			<CssBaseline />
			<ToastContainer />
			<div className={classes.paper}>
				<Avatar className={classes.avatar}></Avatar>
				<Typography component="h1" variant="h5">
					Create New Course
				</Typography>
				<form className={classes.form} noValidate>
					<Grid container spacing={2}>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="name"
								label="Course Name"
								name="name"
								autoComplete="name"
								onChange={handleChange}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="slug"
								label="slug"
								name="slug"
								autoComplete="slug"
								value={courseData.slug}
								onChange={handleChange}
							/>
						</Grid>
						<Grid item xs={12}>
						<Typography>
						Image
						</Typography>
						<input
							accept="image/*"
							className={classes.input}
							id="suqare_image"
							onChange={handleChange}
							name="suqare_image"
							type="file"
						/>
						</Grid>
					</Grid>
					<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="square_image_url"
								label="square_image_url"
								name="square_image_url"
								autoComplete="square_image_url"
								value={courseData.square_image_url}
								onChange={handleChange}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="starts_at"
								label="starts_at"
								name="starts_at"
								autoComplete="starts_at"
								value={courseData.starts_at}
								onChange={handleChange}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="description"
								label="description"
								name="description"
								autoComplete="description"
								value={courseData.description}
								onChange={handleChange}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="instructor"
								label="instructor"
								name="instructor"
								autoComplete="instructor"
								value={courseData.instructor}
								onChange={handleChange}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="category"
								label="category"
								name="category"
								autoComplete="category"
								value={courseData.category}
								onChange={handleChange}
							/>
						</Grid>
					<Button
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						className={classes.submit}
						onClick={handleSubmit}
					>
						Create Course
					</Button>
				</form>
			</div>
		</Container>
	);
}
