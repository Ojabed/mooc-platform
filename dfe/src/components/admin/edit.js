import React, { useState, useEffect } from 'react';
import axiosInstance from '../../axios';
import { useHistory, useParams } from 'react-router-dom';
//MaterialUI
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const useStyles = makeStyles((theme) => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	form: {
		width: '100%', // Fix IE 11 issue.
		marginTop: theme.spacing(3),
	},
	submit: {
		margin: theme.spacing(3, 0, 2),
	},
}));

export default function Create() {
	const history = useHistory();
	const { id } = useParams();
	const initialFormData = Object.freeze({
		id: '',
		name: '',
		slug: '',
		suqare_image: '',
		square_image_url: '',
		starts_at: '',
		description: '',
		instructor: '',
		category: ''
	});

	const [formData, updateFormData] = useState(initialFormData);

	useEffect(() => {
		axiosInstance.get('course/' + id).then((res) => {
			updateFormData({
				...formData,
				['name']: res.data.name,
				['slug']: res.data.slug,
				['suqare_image']: res.data.suqare_image,
				['square_image_url']: res.data.square_image_url,
				['starts_at']: res.data.starts_at,
				['description']: res.data.description,
				['instructor']: res.data.instructor,
				['category']: res.data.category,
			});
		});
	}, [updateFormData]);

	const handleChange = (e) => {
		updateFormData({
			...formData,
			// Trimming any whitespace
			[e.target.name]: e.target.value.trim(),
		});
	};

	const handleSubmit = (e) => {
		e.preventDefault();

		axiosInstance.put(`course/` + id + '/', {
			name: formData.name,
			slug: formData.slug,
			square_image_url: formData.square_image_url,
			starts_at: formData.starts_at,
			description: formData.description,
			instructor: formData.instructor,
			category: formData.category,
		}).then((res) => { 		
			history.push({
				pathname: '/',
			});
			window.location.reload();}).catch((err) => {
			for (var key in err.response.data){
				toast(`${key} : ${err.response.data[key][0]}`)
		}});
	};

	const classes = useStyles();

	return (
		<Container component="main" maxWidth="sm">
			<ToastContainer />
			<CssBaseline />
			<div className={classes.paper}>
				<Typography component="h1" variant="h5">
					Edit Course
				</Typography>
				<form className={classes.form} noValidate>
					<Grid container spacing={2}>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="name"
								label="Course name"
								name="name"
								autoComplete="name"
								value={formData.name}
								onChange={handleChange}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="slug"
								label="slug"
								name="slug"
								autoComplete="slug"
								value={formData.slug}
								onChange={handleChange}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="square_image_url"
								label="square_image_url"
								name="square_image_url"
								autoComplete="square_image_url"
								value={formData.square_image_url}
								onChange={handleChange}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="starts_at"
								label="starts_at"
								name="starts_at"
								autoComplete="starts_at"
								value={formData.starts_at}
								onChange={handleChange}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="description"
								label="description"
								name="description"
								autoComplete="description"
								value={formData.description}
								onChange={handleChange}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="instructor"
								label="instructor"
								name="instructor"
								autoComplete="instructor"
								value={formData.instructor}
								onChange={handleChange}
							/>
						</Grid>
						<Grid item xs={12}>
							<TextField
								variant="outlined"
								required
								fullWidth
								id="category"
								label="category"
								name="category"
								autoComplete="category"
								value={formData.category}
								onChange={handleChange}
							/>
						</Grid>
						
					</Grid>
					<Button
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						className={classes.submit}
						onClick={handleSubmit}
					>
						Update Course
					</Button>
				</form>
			</div>
		</Container>
	);
}
