import React, { useState, useEffect } from 'react';
import axiosInstance from '../../axios';
import { useParams } from 'react-router-dom';
//MaterialUI
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import CardActions from '@material-ui/core/CardActions';
import Box from '@material-ui/core/Box';

function timeConverter(UNIX_timestamp){
	var a = new Date(UNIX_timestamp * 1000);
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	var sec = a.getSeconds();
	var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
	return time;
  }

const useStyles = makeStyles((theme) => ({
	paper: {
		marginTop: theme.spacing(8),
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
}));

export default function Course() {
	const { id } = useParams();
	const classes = useStyles();

	const [data, setData] = useState({
		course: [],
	});

	useEffect(() => {
		axiosInstance.get('course/' + id).then((res) => {
			setData({
				course: res.data,
			});
		});
	}, [setData]);

	return (
		<Container component="main" maxWidth="md">
			<CssBaseline />
			<div className={classes.paper}>
				<Container maxWidth="sm">
					<Paper align="center">
   						<img src={data.course.square_image_url} />
					<Typography
						component="h1"
						variant="h2"
						align="center"
						color="textPrimary"
						gutterBottom
					>
						{data.course.name}{' '}
					</Typography>{' '}
					<Typography
						variant="h5"
						align="center"
						color="textSecondary"
						paragraph
					>
						{data.course.description}{' '}
					</Typography>{' '}

					<CardActions className={classes.cardActions}>
									<Box className={classes.instructor}>
									<Box ml={2}>
										<Typography variant="subtitle2" component="p">
										{data.course.instructor}
										</Typography>
										<Typography variant="subtitle2" color="textSecondary" component="p">
										This course will start at : {timeConverter(data.course.starts_at) }
										</Typography>
									</Box>
									</Box>
									<Box>
									</Box>
					</CardActions>
					</Paper>
				</Container>{' '}
			</div>{' '}
			

		</Container>
	);
}