import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import Link from '@material-ui/core/Link';
import EditIcon from '@material-ui/icons/Edit';


const useStyles = makeStyles((theme) => ({
	card: {
		maxWidth: "100%",
	},
	media: {
		height: 240
	},
	cardActions: {
		display: "flex",
		margin: "0 10px",
		justifyContent: "space-between"
	},
	instructor: {
		display: "flex"
	},

}));

const Courses = (props) => {
	const { courses } = props;
	const classes = useStyles();
	if (!courses || courses.length === 0) return <p>Can not find any courses, sorry</p>;
	return (
		<React.Fragment>
			<Container maxWidth="md" component="main">
				<Grid container spacing={5} alignItems="flex-end">
					{courses.map((course) => {
						return (
							<Grid item xs={12} sm={6} md={4}>
								<Card className={classes.card}>
									<CardActionArea href={'course/' + course.id}>
										<CardMedia
											className={classes.media}
											image={course.square_image_url}
											title={course.slug}
										/>
										<CardContent>
											<Typography gutterBottom variant="h5" component="h2">
												{course.name}
											</Typography>
											<Typography variant="body2" color="textSecondary" component="p">
												{course.description.substr(0, 50)}...
									</Typography>
										</CardContent>
									</CardActionArea>
									<CardActions className={classes.cardActions}>
										<Box className={classes.instructor}>
											<Box ml={2}>
												{localStorage.getItem('access_token') &&
													<Link
														color="textPrimary"
														href={'/admin/delete/' + course.id}
														className={classes.link}
													>
														<DeleteForeverIcon></DeleteForeverIcon>
													</Link>}
												{localStorage.getItem('access_token') &&
													<Link
														color="textPrimary"
														href={'/admin/edit/' + course.id}
														className={classes.link}
													>
														<EditIcon></EditIcon>
													</Link>
												}
											</Box>
										</Box>
										<Box>
										</Box>
									</CardActions>
								</Card>
							</Grid>
						);
					})}
				</Grid>
			</Container>
		</React.Fragment>
	);
};
export default Courses;
