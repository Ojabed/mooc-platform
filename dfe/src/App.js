import React, { useEffect, useState } from 'react';
import './App.css';
import Courses from './components/courses/courses';
import CourseLoadingComponent from './components/courses/courseLoading';
import axiosInstance from './axios';
import Button from '@material-ui/core/Button';


function App() {
	const CourseLoading = CourseLoadingComponent(Courses);
	const [appState, setAppState] = useState({
		loading: true,
		courses: null,
	});
	const [ascOrder, setascOrder] = useState({ordered: false})
	const toggle = () => setascOrder({ordered: !ascOrder.ordered});

	useEffect(() => {
		axiosInstance.get(`/course/?asc_order=${appState.ascOrder}`).then((res) => {
			const allCourses = res.data;
			setAppState({ loading: false, courses: allCourses, ascOrder: !appState.ascOrder });
			
		});
	},[ascOrder.ordered])

	return (
		<div className="App">
			<h1>Learn Or Burn :)</h1>
			<Button
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						onClick={toggle}
					>
						Toggle Course Order
					</Button>
			<CourseLoading isLoading={appState.loading} courses={appState.courses} />
		</div>
	);
}

export default App;
