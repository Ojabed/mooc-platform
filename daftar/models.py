from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

from django_resized import ResizedImageField
from unixtimestampfield.fields import UnixTimeStampField


def get_image_path(instance, filename):
    return 'courses/{filename}'.format(filename=filename)


class Category(models.Model):
    name = models.CharField(max_length=100, null=False, blank=False)
    description = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name_plural = "categories"
        ordering = ['name']

    def __str__(self):
        return self.name


class Course(models.Model):

    class OrderedObjects(models.Manager):
        def get_queryset(self):
            return super().get_queryset().order_by('starts_at')

    name = models.CharField(max_length=250, null=False, blank=False)
    suqare_image = ResizedImageField(
        size=[500, 300], upload_to=get_image_path, blank=True, null=True)
    # square_image in the requirements
    square_image_url = models.URLField(max_length=200)
    starts_at = UnixTimeStampField(use_numeric=True)
    slug = models.SlugField(max_length=250)
    description = models.TextField(null=True, blank=True)
    instructor = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, related_name='instructor', null=True, blank=True)
    category = models.ForeignKey(
        Category, on_delete=models.SET_NULL, null=True, blank=True
    )
    create_date = UnixTimeStampField(auto_now_add=True)

    objects = models.Manager()  # default manager
    ordered_objects = OrderedObjects()  # custom manager

    class Meta:
        unique_together = (('slug', 'category'),)

    def __str__(self):
        return self.name
