from django.urls import path
from django.views.generic import TemplateView

app_name = 'daftar'

urlpatterns = [
    path('', TemplateView.as_view(template_name="daftar/index.html"))
]
