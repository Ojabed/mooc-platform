from django.contrib import admin

from . import models


@admin.register(models.Course)
class CourseAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'slug', 'instructor')
    prepopulated_fields = {'slug': ('name',), }


admin.site.register(models.Category)
