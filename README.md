## About

Daftar is a simple mooc-platform with limited functionalities, it has courses, categories, instructors and students.


## Download and Installation

To begin using this app, choose one of the following options to get started:

* [Clone, or Download this repo]
* Install Python 3.7.9
* Create a new virtualenv called venv: `python3 -m venv venv`
    
If all went well then your command line prompt should now start with (venv).
If your command line prompt does not start with (venv) at this point, try running `source venv/bin/activate`

## Pre-Usage

* Install requirements by running `pip install -r requirements.txt`
* Change directory to app directory `cd assignment/blog`
* Make migrations `python manage.py makemigrations`
* Migrate them `python manage.py migrate`
* Create an admin user `python manage.py createsuperuser`
* Run the server `python manage.py runserver`
* Register an application to get a client id and secret for oAuth `http://127.0.0.1:8000/o/applications/`
* PS: 'nothing fancy, no permissions added, just logging in with no other functionality'
* Relace the secret and client id in the settings `CLIENT_SECRET , CLIENT_ID`
install npm if not present
install required packages by going to `mooc-platform/dfe` and typing `npm install`
run npm by `npm start`

### Basic Usage

To start using Daftar, open localhost `http://127.0.0.1:3000/`
Daftar has the following key-features:


* Login/Register new users
* Prior to logging in, View, edit, create and delete courses
* Reorder courses

### API Calls

After running the server, access the three API end points via the links below (following the default localhost and port `http://127.0.0.1:8000/`)
* List all courses : `api/course/`
* Get a course by id: `api/course/<int:id>/` -- GET
* Create a new course :`api/course/<int:id>/` -- POST
* Edit existing course :`api/course/<int:id>/` -- PUT
* Delete existing course :`api/course/<int:id>/` -- DELETE

Note: Liking an article via an API call can also be done from the Like API Widget located on the right hand-side of the app home page

## Testing

To run tests, run `python manage.py test`
