from django.urls import include, path

from rest_framework import routers

from .views import CourseViewSet

app_name = 'api'
router = routers.DefaultRouter()
router.register('course', viewset=CourseViewSet, basename='cvs')

urlpatterns = [
    path('', include(router.urls))
]
