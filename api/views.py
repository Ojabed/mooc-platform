from daftar.models import Course
from rest_framework import viewsets
from rest_framework.response import Response

from .serializers import CourseSerializer


class CourseViewSet(viewsets.ModelViewSet):
    """
        A viewset for viewing and editing Course instances.
    """

    serializer_class = CourseSerializer
    queryset = Course.objects.all()

    def list(self, request):
        if self.request.query_params.get('asc_order') == 'true':
            courses = Course.ordered_objects.all()
        else:
            courses = Course.objects.all()

        serializer = CourseSerializer(courses, many=True)
        result_set = serializer.data
        return Response(result_set)
