from datetime import datetime

from django.conf import settings

from daftar.models import Course
from rest_framework import serializers


class CourseSerializer(serializers.ModelSerializer):

    def validate(self, data):
        """
        Check that the starts_at is in the future.
        """
        if datetime.utcfromtimestamp(data['starts_at']) < datetime.now():
            raise serializers.ValidationError(
                "starts_at must be in the future")
        return data

    class Meta:
        model = Course
        fields = ('id', 'name', 'suqare_image', 'square_image_url',
                  'starts_at', 'slug', 'description', 'instructor', 'category')
