from django.contrib.auth import get_user_model
from django.urls import reverse
from django.utils import timezone

from daftar.models import Category, Course
from oauth2_provider.models import (get_access_token_model,
                                    get_application_model)
from oauth2_provider.settings import oauth2_settings
from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from users.models import NewUser

Application = get_application_model()
AccessToken = get_access_token_model()
UserModel = get_user_model()


class TestCourseAPI(APITestCase):

    def setUp(self):

        oauth2_settings._SCOPES = [
            "read", "write", "scope1", "scope2", "resource1"]

        self.test_user = NewUser.objects.create_user(
            username="test_user", email="test@user.com", password="123456", first_name='omar')
        self.application = Application.objects.create(
            name="Test Application",
            redirect_uris="http://localhost http://example.com http://example.org",
            user=self.test_user,
            client_type=Application.CLIENT_CONFIDENTIAL,
            authorization_grant_type=Application.GRANT_AUTHORIZATION_CODE,
        )
        self.test_course = Course.objects.create(
            name= "lolololo",
            slug= " lololo",
            starts_at= 99999999999,
            square_image_url= 'https://www.lolo.com')
        self.access_token = AccessToken.objects.create(
            user=self.test_user,
            scope="read write",
            expires=timezone.now() + timezone.timedelta(seconds=300),
            token="secret-access-token-key",
            application=self.application
        )
        # read or write as per your choice
        self.access_token.scope = "read"
        self.access_token.save()

        # correct token and correct scope
        self.auth = "Bearer {0}".format(self.access_token.token)

    def test_success_response(self):

        # Obtaining the POST response for the input data
        response = self.client.get(
            '/api/course/', HTTP_AUTHORIZATION=self.auth)

        # checking wether the response is success
        self.assertEqual(response.status_code, 200)
